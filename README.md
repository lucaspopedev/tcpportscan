# tcpPortScan

Simples scan de portas TCP

## Utilização:

<p> php tcpPortScan.php host rangeInicial-rangeFinal </p>

## Exemplo: 
php tcpPortScan.php bancocn.com 1-100 <br>
Ou <br>
php tcpPortScan.php bancocn.com (Scan com as portas mais utilizadas)