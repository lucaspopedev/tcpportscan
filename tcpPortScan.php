<?php

error_reporting(0);

$address = $argv[1];

$ports = [80, 21, 22, 23, 25, 443, 445, 8080, 8443, 3306, 3389, 8021, 8022, 8023, 8025, 139, 135];

if (!$address) {
    echo PHP_EOL . "Usage: php tcpPortScan.php <ip> <range>" . PHP_EOL . PHP_EOL;
    echo "Example 1: php tcpPortScan.php bancocn.com 1-100" . PHP_EOL;
    echo "Example 2: php tcpPortScan.php bancocn.com" . PHP_EOL . PHP_EOL;
    exit;
}

/**
 * Tenta realzar uma conexão com o endereço e porta informados via socket.
 *
 * @param string $address
 * @param integer $port
 * @return void
 */
function tcpScan(string $address, int $port)
{

    $socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);

    socket_set_option($socket, SOL_SOCKET, SO_SNDTIMEO, array('sec' => 1, 'usec' => 0));

    socket_connect($socket, $address, $port);

    if (socket_last_error($socket) == 0) {
        
        echo "[+] Port {$port} is open" . PHP_EOL;

    } else if (socket_last_error($socket) == -10000 ) {
        
        echo "[-] The domain {$address} is not valid" . PHP_EOL;
        exit;

    } else if (socket_last_error($socket) == 111 || socket_last_error($socket) == 115) {

    } else {

        echo "[-] Unknown error" . PHP_EOL;

    }
}

if ($argv[2]) {
    $rangeToScan = explode("-", $argv[2]);
    
    if (count($rangeToScan) != 2) {
        echo PHP_EOL . "Usage: php tcpPortScan.php <ip> <range>" . PHP_EOL . PHP_EOL;
        echo "Example 1: php tcpPortScan.php bancocn.com 1-100" . PHP_EOL;
        echo "Example 2: php tcpPortScan.php bancocn.com" . PHP_EOL . PHP_EOL;
        exit;
    }

    $rangeInt = count(range($rangeToScan[0], $rangeToScan[1])) + 1;

    for ($i = 0; $i < $rangeInt; $i++) { 
        tcpScan($address, $i);
    }
    
} else {
    foreach ($ports as $port) {
        tcpScan($address, $port);
    }
}
